module.exports=function clientLogic(socket,io){
    console.log('someone connected');    
    socket.on('disconnect',function(){//closing socket connection with chat page 
        console.log("someone disconnected");
    });
    
    socket.on('peerDisconnected',function(data){//emits whenever one peer gets disconnected
        socket.to(data).emit('peerDisconnected');
    })
    
    socket.on('message', function (message) {//send SDP and Ice info to other peer
        socket.in(message.roomName).broadcast.emit('message', message);
        
    });
    
    socket.on('sendMsg',function(message){//sending text messages to other peer
        socket.in(message.roomName).broadcast.emit('receiveMsg',message); 
    });
    
    socket.on('create or join',function(room){//called whenever user joins room      
        console.log("calling "+room);
        if(io.nsps['/chat'].adapter.rooms[room]!=undefined){//checking whether room is created or not
            console.log("room created already");
            if(io.nsps['/chat'].adapter.rooms[room].length <= 2){//perform some action when room is full
                socket.join(room);
                socket.emit('connecting');
                socket.emit('peerLogic',2);
                console.log("adding 2nd user ");
                console.log(io.nsps['/chat'].adapter.rooms[room]);
            }    
        }else{//creating room for first time
            socket.join(room);
            socket.emit('peerLogic',1);
            console.log("adding 1st user");
            console.log(io.nsps['/chat'].adapter.rooms[room]);
        }
    });

}