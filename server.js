"use strict";
var port=process.env.PORT || 8000;
var Express=require("express");//getting express module
var app=Express();

app.set('view engine', 'ejs');
app.use(Express.static("./public"));
app.use(Express.static("./views"));

//getting req for intial page
app.all('/',function(req,res){
});

app.get('/about',function(req,res){//route for handling about page
    res.render('about');
});

app.get('/help',function(req,res){//route for hanling help page
    res.render('help.ejs');    
});

app.get('/privacy',function(req,res){//route for hanling privacy page
    res.render('privacy');
});

app.get('/support',function(req,res){//route for hanling support page
    res.render('support',{title:"Support page is comming soon"});
});

app.get('/:id',function(req,res){
    var id=req.params.id;
    res.render('chat',{roomName:req.params.id});
})

app.post('/support/sendmail',function(req,res){
    req.on('data',function(data){    
        var mail=require('./public/js/mail')(data);
        mail.sendMail(function(bool){
            res.send({bool:bool});
        });
    })
});

var httpServ=app.listen(port,function(){//creating server for application 
    console.log("Sever started");
});

//overriding http protocol with websocket
var io=require('socket.io').listen(httpServ);
var nsp = io.of('/chat');
nsp.on('connection', function(socket){
    let clientLogic=require('./serverLogic');
    console.log(clientLogic);
    clientLogic(socket,io);
    
});


   
