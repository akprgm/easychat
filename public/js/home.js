"use strict";
(function home(){
    
    (function checkSpec(){
        getUserMedia({audio:true, video:true},function(){
            if(!(new RTCPeerConnection(null))){
                alert("Your Web Browser doesn't support webRTC");
                window.location.href="/"+help;
            }
        },function(error) {
            alert("Please install a WebCam or your WebCam is been already used by some other application");
            $('#chatRoomButton').prop('disabled', true);
        });
    })();
   
    var socket=io.connect('/');   
    var chatRoomButton=$('#chatRoomForm').submit(function(e){//creating or joining chat room
        e.preventDefault();
        var chatRoom=$('#chatRoom').val();
        if(chatRoom ===""){
            alert("Please choose a name");
        }else{
            var localStorage=window.localStorage;
            localStorage.setItem('roomName',chatRoom);       
            window.location.href="/"+chatRoom;
         }
    });
})();



