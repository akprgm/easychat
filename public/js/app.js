(function app(){
    (function($){
        $(function(){ 
            var headerHeight = $('.page-header').outerHeight();
     
            $(window).mousemove(function(e) {
                var scrolled = e.pageY;
                if (scrolled < headerHeight+50){
                    $('.page-header').addClass('fixed');
                    $('.page-header').removeClass('off-canvas');
                    
                } else {
                    $('.page-header').addClass('off-canvas');
                    $('.page-header').removeClass('fixed');
                }             
            });
        });
    })(jQuery);

    
    (function checkSpec(){
        getUserMedia({audio:true, video:true},function(){
            if(!(new RTCPeerConnection(null))){
                alert("Your Web Browser doesn't support webRTC");
                window.location.href="/"+help;
            }
        },function(error) {
            alert("Please install WebCam or proper Audio/Video input/output devices");
            $('#chatRoomButton').prop('disabled', true);
        });
    })();//checking whether user has the capability of using our app or not
    
    var localStorage=window.localStorage;
    var caller=false, servers = null , roomName,userName="Tester", localStream, peerConnection,callBtn, hangUpBtn;
    var sdpConstraints = {'mandatory': {
        'OfferToReceiveAudio':true,
        'OfferToReceiveVideo':true 
    }};
    
    
    if(true){// this is where actual code start running
        
        if(!(userName=localStorage.getItem('userName'))){//setting userName in localStorage
            userName=prompt("Please enter a user name");
            localStorage.setItem('userName', userName);
        } 
        if(!(roomName=localStorage.getItem('roomName'))){//setting roomName in localStorage
            roomName=$('#roomName').val();
            localStorage.setItem('roomName',roomName);
        }
        
        callBtn=$('#callBtn');//getttin reference to call button
        hangUpBtn=$('#hangUpBtn');//getting reference to hangup button
        callBtn.prop('disabled', true);
        hangUpBtn.prop('disabled', true);
        callBtn.bind('click',call);//bindind call function to callBtn
        hangUpBtn.bind('click',hangup);//binding hangup function to hang up button
        var socket = io('/chat');
        $('#textBox').keypress(function(event){
           if(event.keyCode ==13){
               event.preventDefault();
               var msg=$('#textBox').val();
               sendTxtMsg(msg);
           } 
        });
        
        socket.on('checkClients',function(data){
           if(data>=2){
               alert("This room name is already taken please choose a different name");
               window.location="../"
           } 
        });
        
        socket.on('peerLogic',function(data){//intializer of whole logic
            if(data===1){
                setLocalVideo();
            }else{
                caller=true;
                console.log("calling because we have 2 users in room");
                setLocalVideo();
                hangUpBtn.prop('disabled', false);    
            }
        });
        
        socket.on("connecting",function(){
            console.log('changing image');
            $('#remoteVideo').attr("poster","/img/connecting.jpg");
        });
        
        socket.on("receiveMsg",function(msg){
            var chatBox=$('#chatMsg');
            var elem='<div id="msgDiv" style="text-align:left" ><aside><p id="msgP" >'+msg.remoteUserName+"</p>"+msg.msg+"</aside></div>";
            chatBox.append(elem);    
        });
        
        socket.emit('create or join',roomName);//joinning chat room 
        
        socket.on('connect',function(){//fired when user connect first time 
            console.log("Client Connected msg form clent");
        });
        
        socket.on('disconnect',function(){//fired when user disconnet to socket connetion
        });
        
        socket.on('peerDisconnected',function(){
            console.log("peer disconnected");
            callBtn.prop('disabled', false);
            hangUpBtn.prop('disabled', true);
            peerConnection.close();
            remoteVideo.src="";
        });
        
        socket.on('full', function (){//fired when chat room is full already has two users
            alert("Sorry this room is already full, Please choose a different roomName");
            //window.location="../";
        });

        socket.on('empty', function (room){//fired when chat room is empty
            isInitiator = true;
            console.log('Room ' + room + ' is empty');
        });

        socket.on('join', function (room){//fired when someone join chatRoom
            console.log('Making request to join room ' + room);
            console.log('You are the initiator!');
        });
        
                
        socket.on('message', function (message){//getting sdp and ice candidates 
            console.log('Client received message:', message);
            if (message.type === 'offer') {
                console.log("got remote description from caller");
                hangUpBtn.prop('disabled', false);
                peerConnection = new RTCPeerConnection(servers,{optional: [{RtpDataChannels: true}]});
                peerConnection.setRemoteDescription(new RTCSessionDescription(message.description));
                peerConnection.addStream(localStream);
                peerConnection.onaddstream=addRemoteStream;
                createAnswer();
            } else if (message.type === 'answer') {
                console.log("getting back the answer from callee");
                peerConnection.setRemoteDescription(new RTCSessionDescription(message.description));
                console.log(message.description);
            } else if (message.type === 'candidate') {
                console.log("m i actually getting ice candidates");
                peerConnection.addIceCandidate(new RTCIceCandidate(message.ice));
            } else if (message === 'bye' && isStarted) {
                handleRemoteHangup();
            }
        });
        
    }
    
    
    window.onbeforeunload=function(){
        localStorage.removeItem('roomName');
        peerConnection.close();
        socket.emit('peerDisconnected',roomName);
    }
    
    function handleError(){}
    
    function sendTxtMsg(msg){
        var chatBox=$('#chatMsg');
        var elem='<div id="msgDiv"><aside><p id="msgP" >'+userName+"</p>"+msg+"</aside></div>";
        chatBox.append(elem); 
        socket.emit('sendMsg',{
            msg:msg,
            roomName:roomName,
            remoteUserName:userName
            });
        $('#textBox').val("");    
    }
    
    function setLocalVideo(){//setting local video as soon as user comes to chat page
        var localVideo = $("#localVideo");
        var remoteVideo = $("#remoteVideo");
        console.log("Requesting local stream");
        getUserMedia({audio:true, video:true}, gotStream,function(error) {
            console.log("getUserMedia error: ", error);
        });
    }
    
    function gotStream(stream){//setting local stream to local video element 
        
        console.log("Received local stream");
        
        localVideo.src = URL.createObjectURL(stream);
        localStream = stream;       
        console.log(localStream);
        if (localStream.getVideoTracks().length > 0) {
            console.log('Using video device: ' + localStream.getVideoTracks()[0].label);
        }
        if (localStream.getAudioTracks().length > 0) {
            console.log('Using audio device: ' + localStream.getAudioTracks()[0].label);
        }
        console.log("Created local peer connection object peerConnection");
        peerConnection = new RTCPeerConnection(servers,{optional: [{RtpDataChannels: true}]});        
        if(caller)//executing call if this is caller
            call();
    }
    
    function call() {//creating peer connection object and attaching local stream to it
        peerConnection = new RTCPeerConnection(servers,{optional: [{RtpDataChannels: true}]});
        console.log("Now calling my call function");
        peerConnection.onicecandidate = gotLocalIceCandidate;
        peerConnection.onaddstream=addRemoteStream;  
        peerConnection.addStream(localStream);
        console.log("Added localStream to peerConnection");
        if(caller)
            peerConnection.createOffer(gotLocalDescription,handleError);
        
    }
    
    function gotLocalDescription(description){//setting local sdp to peer object and sending the sdp to other client
        peerConnection.setLocalDescription(description);
        console.log('getting local description and sendig offer');
        socket.emit('message',{description:description,roomName:roomName,type:"offer"});
    }
    
    function createAnswer() {//callee creating answer when sdp recieved which is sent by caller
        console.log('Sending answer to peer.');
        peerConnection.createAnswer(function(sessionDescription){          
            peerConnection.setLocalDescription(sessionDescription);
            console.log('seding description to caller');
            socket.emit('message',{
                description:sessionDescription,
                roomName:roomName,
                type:"answer"
            });
        }, function(){
            console.log("error occured while created offer");
        }, sdpConstraints);
    }
    
    function addRemoteStream(event){//adding remote stream to remote video element this function will be called when remote stream is available
        console.log("adding remote stream after doing every shitty work");
        if (!event)
            console.log("kya chutiyap ");
        remoteVideo.src = URL.createObjectURL(event.stream);
    }

    function gotLocalIceCandidate(event){//getting local ice candidates and sending them each time when an ice is available
        if (event.candidate) {
            console.log("printing ice event"+event ); 
            socket.emit('message',{
                ice: event.candidate,
                roomName:roomName,
                type: 'candidate'
            });
        } else {
            console.log('End of candidates.');
        }
    }

    function hangup() {//hangup function is for stopping the call
        console.log("Ending call");
        peerConnection.close();
        callBtn.prop('disabled', false);
        hangUpBtn.prop('disabled', true);
        remoteVideo.src="";
        socket.emit('peerDisconnected',roomName);
    }
    
        
       
})();