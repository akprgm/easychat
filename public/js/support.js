(function support(){
    $('#feedBackForm').bind('submit',function(e){
        e.preventDefault();
        $('#errorText').text('');
        var name=$('#userName').val();
        var email=$('#userEmail').val();
        var feedback=$('#userQuery').val();
        var ob={
            'uname':name,
            'uemail':email,
            'ufeedback':feedback
        }
        console.log(ob);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "support/sendmail", //Relative or absolute path to response.php file
            data: JSON.stringify(ob),
            success: function(data) {
                if(data.bool){
                    $('#errorSymbol').removeClass("errorSymbolWrong fa-close");
                    $('#errorSymbol').addClass("errorSymbolRight fa-check");
                    $('#errorText').text("  Your feedback is been recieved Sucessfully");
                    $('#errorMsg').removeClass("hide");
                    document.getElementById('feedBackForm').reset();            
                }else{
                    $('#errorSymbol').removeClass("errorSymbolRight fa-check");
                    $('#errorSymbol').addClass("errorSymbolWrong fa-close");
                    $('#errorText').text("  Please enter a valid email Id or check your connection");
                    $('#errorMsg').removeClass("hide");
                }    
            }
        });  
    });   
})();